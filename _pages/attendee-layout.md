---
layout: single
title: "Attendee Layout"
permalink: /node/14
---

Morning, Saturday 2008-07-19

| -------------- | -------------- |
| Michael Dexter (BSDFund.org, LinuxFund.org) | |
| Henri Yandell (Apache) |         Kaliya Hamlin (Identity Commons) |
| Don Smith (Eclipse.org) |        Scott Rainey (LinuxFund.org) |
| Luis Villa (legal, GNOME) |      Gerv Markham (Mozilla) |
| Jacob Kaplan-Moss (Django) |     Karl Fogel (questioncopyright.org, Subversion) |
| Dave Neary (GNOME) |             Zak Greant (Mozilla) |
| Allison Randal (Perl, Parrot) |   Bradley Kuhn (Conservancy / SFLC) |
| Karen Sandler (SFLC lawyer) |    Josh Berkus (SPI / PostgreSQL) |
| Lance Albertson (OSU OSL) |      David Mandel (LinuxFund.org, PLUG)  |

Afternoon, Saturday 2008-07-19

| -------------- | -------------- |
| Michael Dexter (BSDFund.org, LinuxFund.org) | |
| Henri Yandell (Apache) |         Kaliya Hamlin (Identity Commons) |
| Don Smith (Eclipse.org) |        Scott Rainey (LinuxFund.org) |
| Luis Villa (legal, GNOME) |       Gerv Markham (Mozilla) |
| Jacob Kaplan-Moss (Django) |     Zak Greant (Mozilla) |
| Allison Randal (Perl, Parrot) |  Bradley Kuhn (Conservancy / SFLC) |
| Lance Albertson (OSU OSL) |      Josh Berkus (SPI / PostgreSQL) |
| Karen Sandler (SFLC lawyer) |    David Mandel (LinuxFund.org, PLUG)                 |
| Dave Neary (GNOME) |             Karl Fogel (questioncopyright.org, Subversion) |

Afternoon session 2, Saturday 2008-07-19

| -------------- | -------------- |
| Michael Dexter (BSDFund.org, LinuxFund.org) | |
| Henri Yandell (Apache) |         Kaliya Hamlin (Identity Commons) |
| Don Smith (Eclipse.org) |        Scott Rainey (LinuxFund.org) |
| Alex Russell (Dojo) |            Gerv Markham (Mozilla) |
| Jacob Kaplan-Moss (Django) |     Zak Greant (Mozilla) |
| Dave Neary (GNOME) | |
| Allison Randal (Perl, Parrot) |  Bradley Kuhn (Conservancy / SFLC) |
| Lance Albertson (OSU OSL) |      Josh Berkus (SPI / PostgreSQL) |
| Karen Sandler (SFLC lawyer) |    David Mandel (Linux Fund, PLUG)                 |
| Danese Cooper (OSI) |            Karl Fogel (questioncopyright.org, Subversion) |
| Simon Phipps (Sun) |             Alex Russell (Dojo) |

Morning session 1 Sunday 2008-07-20

| -------------- | -------------- |
| Michael Dexter (BSD Fund, Linux Fund) | |
| Luis Villa (GNOME) | |
| Don Smith (Eclipse.org) |          Scott Rainey (LinuxFund.org) |
| Karen Sandler (SFLC, lawyer) |     Henri Yandell (Apache) |
| Dave Neary (GNOME) |               Bradley Kuhn (Conservancy, SFLC) |
| Allison Randal (Perl, Parrot) |    Aaron Williamson (SFLC, lawyer) |
| Scott Kveton OSL |                 David Mandel (LinuxFund.org, PLUG) |
| Jacob Kaplan-Moss (Django) |      Greg Lund-Chaix (OSU OSL) |
| Simon Phipps (Sun Microsystems, Inc.) |   Karl Fogel (Questioncopyright.org / SVN) |
| Derek Keats (African Access Project?) | |
| Danese Cooper (Intel, OSI) |       Zak Greant (Mozilla / OSI / FSF) |
| Alex Russell (Dojo) | |
